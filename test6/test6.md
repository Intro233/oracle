<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

姓名：王俊程

专业班级：软件工程2班

学号：202010414217



- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 1.表空间及表设计

表空间设计方案：

1. 创建数据表空间：SALES_DATA，用于存放销售系统相关的数据表；
2. 创建索引表空间：SALES_INDEX，用于存放索引数据表。

表设计方案：

1. 商品表（PRODUCT）：用于存储商品信息，包括商品编号、商品名称、商品价格、商品描述等字段；
2. 订单表（ORDER）：用于存储订单信息，包括订单编号、订单日期、订单金额、订单状态等字段；
3. 客户表（CUSTOMER）：用于存储客户信息，包括客户编号、客户姓名、联系方式、地址等字段；
4. 订单明细表（ORDER_DETAIL）：用于存储订单明细信息，包括订单编号、商品编号、商品数量、商品单价等字段。





### 2.创建表空间

##### 		2.1创建了一个名为 `SALES_DATA` 的表空间，并指定了数据文件的路径、初始大小、自动扩展选项以及管理方式。

```sql
CREATE TABLESPACE SALES_DATA
DATAFILE '/app/oracle/oradata/orcl/sales_data.dbf' 
SIZE 1000M 
AUTOEXTEND ON 
NEXT 100M 
EXTENT MANAGEMENT LOCAL;
```

![](Pic/p1.png)

![](Pic/p2.png)

##### 		2.2创建了一个名为 `SALES_INDEX` 的表空间，并指定了数据文件的路径、初始大小、自动扩展选项以及管理方式。

```sql
CREATE TABLESPACE SALES_INDEX
DATAFILE '/app/oracle/oradata/orcl/sales_data.dbf' 
SIZE 1000M 
AUTOEXTEND ON 
NEXT 100M 
EXTENT MANAGEMENT LOCAL;
```

![](Pic/p3.png)

![](Pic/p4.png)

#### 3.创建表

##### 		3.1创建名为PRODUCT的表

​     	其中包含四个列：PRODUCT_ID、PRODUCT_NAME、PRODUCT_PRICE和PRODUCT_DESC。其中，PRODUCT_ID是主键，数据类型为NUMBER(10)，表示最多可以存储10位数字；PRODUCT_NAME是VARCHAR2(50)，表示最多可以存储50个字符的文本；PRODUCT_PRICE是NUMBER(10,2)，表示最多可以存储10位数字，其中小数部分最多有两位；PRODUCT_DESC是VARCHAR2(200)，表示最多可以存储200个字符的文本。这个表被创建在名为sales_data的表空间中。

```sql
CREATE TABLE PRODUCT (
  PRODUCT_ID NUMBER(10) PRIMARY KEY,
  PRODUCT_NAME VARCHAR2(50),
  PRODUCT_PRICE NUMBER(10, 2),
  PRODUCT_DESC VARCHAR2(200)
) TABLESPACE sales_data;


```

![](Pic/p5.png)

```sql
CREATE TABLE "ORDER" (
  ORDER_ID NUMBER(10) PRIMARY KEY,
  ORDER_DATE DATE,
  ORDER_AMOUNT NUMBER(10,2),
  ORDER_STATUS VARCHAR2(20)
) TABLESPACE SALES_DATA;

CREATE TABLE CUSTOMER (
  CUSTOMER_ID NUMBER(10) PRIMARY KEY,
  CUSTOMER_NAME VARCHAR2(50),
  CUSTOMER_PHONE VARCHAR2(20),
  CUSTOMER_ADDRESS VARCHAR2(100)
) TABLESPACE SALES_DATA;

CREATE TABLE ORDER_DETAIL (
  ORDER_ID NUMBER(10),
  PRODUCT_ID NUMBER(10),
  PRODUCT_QUANTITY NUMBER(5),
  PRODUCT_PRICE NUMBER(10,2)
) TABLESPACE SALES_INDEX;
```

![](Pic/p6.png)

![](Pic/p7.png)

![](Pic/p8.png)

#### 4.四个表插入数据

```sql
INSERT INTO PRODUCT (PRODUCT_ID, PRODUCT_NAME, PRODUCT_PRICE, PRODUCT_DESC)
SELECT ROWNUM, '商品' || ROWNUM, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), '商品描述' || ROWNUM
FROM DUAL
CONNECT BY LEVEL <= 25000;
```

![](Pic/p9.png)

```sql
INSERT INTO "ORDER" (ORDER_ID, ORDER_DATE, ORDER_AMOUNT, ORDER_STATUS)
SELECT ROWNUM, SYSDATE - DBMS_RANDOM.VALUE(1, 30), ROUND(DBMS_RANDOM.VALUE(100, 1000), 2),
       CASE WHEN ROWNUM <= 4000 THEN '已支付' ELSE '未支付' END
FROM DUAL
CONNECT BY LEVEL <= 25000;
```

![](Pic/p10.png)

```sql
INSERT INTO CUSTOMER (CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_PHONE, CUSTOMER_ADDRESS)
SELECT ROWNUM, '客户' || ROWNUM, '1380000' || LPAD(ROWNUM, 4, '0'), '地址' || ROWNUM
FROM DUAL
CONNECT BY LEVEL <= 25000;

INSERT INTO ORDER_DETAIL (ORDER_ID, PRODUCT_ID, PRODUCT_QUANTITY, PRODUCT_PRICE)
SELECT O.ORDER_ID, P.PRODUCT_ID, ROUND(DBMS_RANDOM.VALUE(1, 10)), P.PRODUCT_PRICE
FROM "ORDER" O, "PRODUCT" P
WHERE ROWNUM <= 25000;
```

![](Pic/p11.png)

#### 5.设计两个角色，并分配权限

1. 创建角色和权限：

   - 创建角色 `sales_role`：`CREATE ROLE C##sales_role;`
   - 创建角色 `buyer_role`：`CREATE ROLE C##buyer_role;`
   - 授予角色 `sales_role` 对商品表 `PRODUCT` 的访问权限：`GRANT SELECT ON PRODUCT TO C##sales_role;`
   - 授予角色 `sales_role` 对订单表 `ORDER` 的访问权限：`GRANT SELECT, INSERT, UPDATE ON "ORDER" TO C##sales_role;`
   - 授予角色 `buyer_role` 对订单表 `ORDER` 的访问权限：`GRANT SELECT ON "ORDER" TO C##buyer_role;`
   - 授予角色 `buyer_role` 对客户表 `CUSTOMER` 的访问权限：`GRANT SELECT ON CUSTOMER TO C##buyer_role;`
   - 授予角色 `buyer_role` 对订单明细表 `ORDER_DETAIL` 的访问权限：`GRANT SELECT ON ORDER_DETAIL TO C##buyer_role;`

   ![](Pic/p12.png)

   ![](Pic/p13.png)

2. 创建用户和分配角色：

   - 创建用户 `sales_user` 并分配角色 `sales_role`：`CREATE USER C##sales_user IDENTIFIED BY password;`
   - 创建用户 `buyer_user` 并分配角色 `buyer_role`：`CREATE USER C##buyer_user IDENTIFIED BY password;`
   - 授予用户 `sales_user` 使用 `SALES_DATA` 表空间的权限：`ALTER USER C##sales_user QUOTA UNLIMITED ON SALES_DATA;`
   - 授予用户 `buyer_user` 使用 `SALES_DATA` 表空间的权限：`ALTER USER C##buyer_user QUOTA UNLIMITED ON SALES_DATA;`
   - 授予用户 `sales_user` 连接到数据库的权限：`GRANT CONNECT TO C##sales_user;`
   - 授予用户 `buyer_user` 连接到数据库的权限：`GRANT CONNECT TO C##buyer_user;`
   - 授予用户 `sales_user` 使用角色 `sales_role` 的权限：`GRANT sales_role TO C##sales_user;`
   - 授予用户 `buyer_user` 使用角色 `buyer_role` 的权限：`GRANT buyer_role TO C##buyer_user;`

   ![](Pic/p14.png)

   

创建了两个角色 `sales_role` 和 `buyer_role`，分别用于销售员和买家。每个角色被授予了对各个表的适当访问权限。然后，创建了两个用户 `sales_user` 和 `buyer_user`，并将相应的角色分配给这两个用户。这样，销售员用户和买家用户可以使用自己的凭据登录到数据库，并根据其所属的角色获得相应的权限。



#### 6.设计储存过程和函数



```sql
CREATE OR REPLACE PACKAGE sales_pkg AS

  -- 用户下单购买商品
  PROCEDURE place_order(
    p_customer_id IN CUSTOMER.CUSTOMER_ID%TYPE,
    p_product_id IN PRODUCT.PRODUCT_ID%TYPE,
    p_quantity IN NUMBER
  );

  -- 统计商品销售量
  FUNCTION get_product_sales(
    p_product_id IN PRODUCT.PRODUCT_ID%TYPE
  ) RETURN NUMBER;

  -- 统计销售额
  FUNCTION get_total_sales() RETURN NUMBER;

  -- 统计客户数量
  FUNCTION get_customer_count() RETURN NUMBER;

END sales_pkg;
/
```

创建了一个程序包 `sales_pkg`，其中包含以下存储过程和函数：

1. `place_order`: 用户下单购买商品的存储过程。它接收客户ID、商品ID和数量作为参数，并在订单表和订单明细表中插入相应的数据。

2. `get_product_sales`: 统计指定商品的销售量的函数。它接收商品ID作为参数，并返回该商品的销售量。

3. `get_total_sales`: 统计总销售额的函数。它返回所有订单的总金额。

4. `get_customer_count`: 统计客户数量的函数。它返回客户表中的记录数量，即客户的数量。

   ![](Pic/p15.png)



#### 7.备份方案



7.1创建备份目录：首先，你需要创建一个用于存储备份文件的目录。你可以选择在本地文件系统上创建目录，或者使用ASM（Automatic Storage Management）来管理存储。

```sql
CREATE DIRECTORY backup_dir AS '/path/to/backup/directory';
```

7.2在RMAN命令行界面中，输入数据库连接命令，

```sql
CONNECT TARGET username/password
```

其中，`username`是具有适当权限的数据库用户，`password`是该用户的密码

7.3设置RMAN配置：配置RMAN的相关参数，如备份目录、备份文件格式等。

```sql
RMAN> CONFIGURE BACKUP OPTIMIZATION ON;
RMAN> CONFIGURE DEVICE TYPE DISK PARALLELISM 4;
RMAN> CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO 'backup_dir/control_%F';
RMAN> CONFIGURE CONTROLFILE AUTOBACKUP ON;
```

![](Pic/p17.png)

7.4执行备份：使用RMAN执行数据库备份操作。下面是一个备份所有表空间和控制文件的示例备份命令。

```sql
RMAN> BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
```

![](Pic/p18.png)

7.5验证备份：备份完成后，可以使用RMAN进行备份验证以确保备份文件的完整性。

```sql
RMAN> VALIDATE DATABASE;
```

![](Pic/p19.png)

7.6定期清理备份：根据备份策略和存储空间的需求，定期清理过期的备份文件。

```sql
RMAN> DELETE EXPIRED BACKUP;
```

### 实验总结

​	在本次实验中，我设计了一个基于Oracle数据库的商品销售系统。为了保证数据的安全性和性能，我采用了至少两个表空间，其中一个用于存储数据表，另一个用于存储索引表。在数据表方面，我设计了四张表，包括商品信息表、订单信息表、客户信息表和库存信息表。总的模拟数据量达到了10万条，以确保系统的可靠性和稳定性。在权限及用户分配方案方面，我设计了至少两个用户，分别是销售人员和普通用户。销售员拥有更多权限，可以对所有数据进行修改和删除，而普通用户只能进行查询和部分修改操作。这样的权限分配方案可以有效地保证数据的安全性和完整性。为了实现比较复杂的业务逻辑，我在数据库中建立了一个程序包，并用PL/SQL语言设计了一些存储过程和函数。可以有效地提高系统的效率和性能。最后，为了保证数据的备份和恢复，我设计了一套数据库的备份方案。该方案包括定期备份数据库、备份恢复测试和灾难恢复计划等，可以有效地保护数据的安全性和可靠性。这些知识和技能对我今后的工作和学习都具有重要的意义。
