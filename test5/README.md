# 实验5：包，过程，函数的用法

- 学号：202010414217 
- 姓名：王俊程 
- 班级：2020级2班

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

- 创建一个包(Package)，包名是MyPack。

- 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。

- 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
  Oracle递归查询的语句格式是：

  ```sql
  SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
  START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
  CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
  ```

## 实验步骤

##### 步骤1：创建包MyPack

```sql
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
```

![](pic1.png)

##### 步骤2：创建一个函数Get_SalaryAmoun，统计每个部门的salay工资总额，并且创建过程GET_EMPLOYEES

```sql
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
```

![](pic2.png)

##### 步骤三：测试步骤二的Get_SalaryAmoun函数

```sql
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

```

![](pic3.png)

##### 步骤四：测试步骤二的GET_EMPLOYEES过程

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
```

![](pic4.png)

## 结论

- 在实验中，我首先了解了PL/SQL语言的基本结构，包括块、语句、变量和常量。我学习了如何声明和使用变量和常量，以及如何进行算术和逻辑运算。我还学习了如何使用条件语句和循环语句来控制程序流程。我学了包、过程和函数的用法。包是一种组织PL/SQL程序的方式，可以将相关的程序组织在一起，方便管理和维护。过程和函数是PL/SQL程序的模块化单元，可以被其他程序调用。在实验中，我学习了如何定义和调用过程和函数，以及如何使用参数传递数据。这些知识和技能将对我今后的工作和学习有很大的帮助。
