- # 实验2：用户及权限管理

  姓名：王俊程   学号：202010414217   班级：软工2班

  ## 实验目的

  掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

  ## 实验内容

  Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

  - 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
  - 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
  - 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

  ## 实验步骤

  ##### 步骤1

  - 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

  ```sql
  $ sqlplus system/123@pdborcl
  CREATE ROLE con_res_role;
  GRANT connect,resource,CREATE VIEW TO con_res_role;
  CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
  ALTER USER sale default TABLESPACE "USERS";
  ALTER USER sale QUOTA 50M ON users;
  GRANT con_res_role TO sale;
  --收回角色
  REVOKE con_res_role FROM sale;
  ```

  ![](p1.png)

  ##### 步骤2

  - 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。
  
  ```sql
  $ sqlplus sale/123@pdborcl
  SQL> show user;
  USER is "sale"
  SQL>
  SELECT * FROM session_privs;
  SELECT * FROM session_roles;
  SQL>
  CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;
  INSERT INTO customers(id,name)VALUES(1,'zhang');
  INSERT INTO customers(id,name)VALUES (2,'wang');
  CREATE VIEW customers_view AS SELECT name FROM customers;
  GRANT SELECT ON customers_view TO hr;
  SELECT * FROM customers_view;
  NAME
  --------------------------------------------------
  zhang
  wang
  ```

  ![](p2.png)
  
  ##### 步骤3
  
  用户hr连接到pdborcl，查询sale授予它的视图customers_view
  
  ```sql
  $ sqlplus hr/123@pdborcl
  SQL> SELECT * FROM sale.customers;
  elect * from sale.customers
                     *
  第 1 行出现错误:
  ORA-00942: 表或视图不存在
  
  SQL> SELECT * FROM sale.customers_view;
  NAME
  --------------------------------------------------
  zhang
  wang
  ```
  
  ![](p3.png)
  
  > 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
  > sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。
  
  概要文件设置,用户最多登录时最多只能错误3次

  ```sql
  $ sqlplus system/123@pdborcl
  SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
  ```

  - 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
  - 锁定后，通过system用户登录，alter user sale unlock命令解锁。
  
  ```sh
  $ sqlplus system/123@pdborcl
  SQL> alter user sale  account unlock;
  ```

  ![](p4.png)

  ##### 步骤4
  
  查看数据库的使用情况
  
  以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。
  
  ```sql
  $ sqlplus system/123@pdborcl
  
  SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';
  
  SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
   free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
   Round(( total - free )/ total,4)* 100 "使用率%"
   from (SELECT tablespace_name,Sum(bytes)free
          FROM   dba_free_space group  BY tablespace_name)a,
         (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
          group  BY tablespace_name)b
   where  a.tablespace_name = b.tablespace_name;
  ```
  
  ![](p5.png)
  
  ![](p6.png)
  
  ##### 步骤5

  实验结束删除用户和角色

  ```sh
  $ sqlplus system/123@pdborcl
  SQL>
  drop role con_res_role;
  drop user sale cascade;
  ```
  
  ![](p7.png)
  
  ## 实验总结
  
  通过本次实验，我掌握了在Oracle数据库中创建角色和用户，并授权角色给用户的方法，也学会了如何测试用户的权限并进行相关操作。
